<?php

/**
 * @file
 *   Admin functions for logo_switcher module.
 */

/**
 * Admin settings form.
 *
 * Some code taken from system_theme_settings().
 */
function logo_switcher_admin_settings() {
  $form = array();
  $form['#attributes'] = array('enctype' => 'multipart/form-data');
  $form['#submit'] = array('logo_switcher_admin_settings_submit');

  $form['logo_switcher_logo'] = array(
    '#type' => 'textfield',
    '#title' => t('Path to custom logo'),
    '#default_value' => variable_get('logo_switcher_logo', ''),
    '#description' => t('The path to the file you would like to use as your alternate logo file.')
  );

  $form['logo_upload'] = array(
    '#type' => 'file',
    '#title' => t('Upload logo image'),
    '#maxlength' => 40,
    '#description' => t("If you don't have direct file access to the server, use this field to upload your logo.")
  );

  $form['logo_switcher_pages'] = array(
    '#type' => 'textarea',
    '#title' => t('Pages'),
    '#default_value' => variable_get('logo_switcher_pages', ''),
    '#description' => t('Enter one page per line as Drupal paths. The '*' character is a wildcard. Example paths are blog for the blog page and blog/* for every personal blog. <front> is the front page.'),
  );

  return system_settings_form($form);
}

function logo_switcher_admin_settings_submit($form, &$form_state) {
  $directory_path = file_directory_path();
  file_check_directory($directory_path, FILE_CREATE_DIRECTORY, 'file_directory_path');

  // Check for a new uploaded logo, and use that instead.
  if ($file = file_save_upload('logo_upload', array('file_validate_is_image' => array()))) {
    $parts = pathinfo($file->filename);
    $filename = 'switched_logo.'. $parts['extension'];

    // The image was saved using file_save_upload() and was added to the
    // files table as a temporary file. We'll make a copy and let the garbage
    // collector delete the original upload.
    if (file_copy($file, $filename, FILE_EXISTS_REPLACE)) {
      $form_state['values']['logo_switcher_logo'] = $file->filepath;
    }
  }

  unset($form_state['values']['logo_upload']);
}
